
typedef struct Node {
    int data;

    // Lower values indicate higher priority 
    int priority;

    struct Node* next;

}Node;

int peek(Node** head);
void pop(Node** head);
void push(Node** head, int d, int p);
int isEmpty(Node** head);
Node* newNode(int d, int p);
void update(Node** head, int id, int v);
int Contains(Node** head, int i);