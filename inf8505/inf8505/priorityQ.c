
// C code to implement Priority Queue 
// using Linked List 
#include <stdio.h> 
#include <stdlib.h> 
#include "priorityQ.h"

 
// Function to Create A New Node 
Node* newNode(int d, int p) 
{ 
    Node* temp = (Node*)malloc(sizeof(Node)); 
    temp->data = d; 
    temp->priority = p; 
    temp->next = NULL; 
 
    return temp; 
} 
 
// Return the value at head 
int peek(Node** head) 
{ 
    return (*head)->data; 
} 
 
// Removes the element with the 
// highest priority form the list 
void pop(Node** head) 
{ 
    Node* temp = *head; 
    (*head) = (*head)->next; 
    free(temp); 
} 
 
// Function to push according to priority 
void push ( Node** head, int d, int p) 
{ 
    Node* start = (*head); 
 
    // Create new Node 
    Node* temp = newNode(d, p); 
    
    if (!(*head))
    {
        (*head) = temp;
    }
    else {
        if ((*head)->priority >= p)
        {
            // Special Case: The head of list has lesser 
            // priority than new node. So insert new 
            // node before head node and change head node. 

            // Insert New Node before head 
            temp->next = *head;
            (*head) = temp;
        }
        else {

            // Traverse the list and find a 
            // position to insert new node 
            while (start->next != NULL &&
                start->next->priority < p) {
                start = start->next;
            }

            // Either at the ends of the list 
            // or at required position 
            temp->next = start->next;
            start->next = temp;
        }
    }
} 
 
// Function to check is list is empty 
int isEmpty(Node** head) 
{ 
    return (*head) == NULL; 
} 
 
int Contains(Node** head, int i)
{
    Node* n= *head;
    while (!(n==NULL))
    {

        if (n->data == i)
            return 1;
        else
        n = n->next;
            
    }
    return 0;
}

void update(Node** head, int id, int v)
{
    Node* crawler = *head;
    while (crawler!=NULL)
    {
        if (crawler == *head && crawler->data==id)
        {
            if (v < crawler->priority)
            {
                crawler->priority = v;
                
            }
            break;
        }
        else if (crawler->next->data == id)
        {
            if (crawler->next->priority > v)
            {

                Node* temp = crawler->next;
                crawler->next = temp->next;
                push(head, temp->data, v);
                //free(temp);
            }
            break;
        }
        crawler = crawler->next;
    }
}