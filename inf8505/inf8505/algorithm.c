#include <math.h>
#include <stdio.h>
#include "priorityQ.h"
#include "benchmark_data.h"
#include <string.h>

#define GENERATE_OUTPUT


//#define M  100 // nombre global, largeur de notre grille de nodes, donc 25 Nodes
//#define N  M*M //nombre de nodes
int M = 100;
int N;


int generate_output(int* parents, int start)
{
    int crawler = parents[GOAL];
    FILE* fp;
    fp = fopen("output.dat", "w");
    if (fp == NULL) { return -1; };
    while (crawler != start)
    {
        fprintf(fp,"%d\n", crawler);
        crawler = parents[crawler];
    }
    fprintf(fp, "%d\n", crawler);
    fclose(fp);
    return 0;
}

unsigned short getHeuristic(short node)
{
    return benchmark_heuristics[node];
}

int getNeighbours( int node, int* neighbours)
{
    memcpy(neighbours,benchmark_neighbours[node],4*sizeof(int));
}

int main(){
   N = M * M;
  int noeud = 0;
  short costs[NODE_NUM];
  memset(costs, -1, sizeof(costs));

  
  int parents[NODE_NUM];
  memset(parents, -1, sizeof(parents));

  costs[START] = 0;



  int goal=GOAL;
  int start = START;

  
  Node* pq = newNode(start, 0);
 // pq_push(start,0);
 // while (!pq_is_empty())
  while (!isEmpty(&pq))
  {
      int node = peek(&pq); 
      pop(&pq);
      //int node=pq_pop();
      //if (node!=node_temp) printf("error! %d, %d\n",node, node_temp);
      if (node == goal) {
#ifdef GENERATE_OUTPUT
          generate_output(parents,start);
#endif
          //print_matrix(parents, M);
          return 0;
      }
      
      int* neighbours=benchmark_neighbours[node];
      //getNeighbours( node, neighbours);

      for (int i = 0; i < 4; i++)
      {
          int neighbour_n = neighbours[i];
          unsigned short score = costs[node]+1;
          if (neighbour_n>=0 && score < (unsigned short)costs[neighbour_n])
          {
              parents[neighbour_n] = node;
              costs[neighbour_n] = score;
              //if (!pq_contains(neighbour_n))
              if(!Contains(&pq, neighbour_n))
              {
                //  if(pq_contains(neighbour_n)) printf(" n contains : %d\n",pq_contains(neighbour_n));
                //pq_push(neighbour_n,costs[neighbour_n] + getHeuristic(neighbour_n));
                push(&pq, neighbour_n, costs[neighbour_n] + benchmark_heuristics[neighbour_n]);
              }
            else
            {
              //if (!pq_contains(neighbour_n))  printf(" y contains : %d\n",pq_contains(neighbour_n));
              
              update(&pq, neighbour_n, costs[neighbour_n]+ benchmark_heuristics[neighbour_n]);
              //pq_update(neighbour_n, costs[neighbour_n]+ getHeuristic(neighbour_n));

            }
          }

      }

  }

  return -1;
}
